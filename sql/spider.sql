/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : spider

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-06-24 20:27:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hotel
-- ----------------------------
DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel` (
  `hotel_id` varchar(15) NOT NULL DEFAULT '',
  `hotel_name` varchar(100) DEFAULT NULL,
  `hotel_strategymedal` int(1) DEFAULT NULL COMMENT '1:是；0：不是（携程战略合作酒店，拥有优质服务、优良品质及优惠房价，供携程会员专享预订）',
  `hotel_address` varchar(300) DEFAULT NULL,
  `hotel_price` int(11) DEFAULT NULL,
  `hotel_special_label` varchar(100) DEFAULT NULL,
  `hotel_facility` varchar(100) DEFAULT NULL,
  `hotel_level` varchar(50) DEFAULT NULL,
  `hotel_value` float DEFAULT NULL,
  `hotel_total_judgement_score` int(2) DEFAULT NULL,
  `hotel_judgement` int(11) DEFAULT NULL,
  `hotel_recommend` varchar(100) DEFAULT NULL,
  `page_index` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hotel_comment
-- ----------------------------
DROP TABLE IF EXISTS `hotel_comment`;
CREATE TABLE `hotel_comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` varchar(15) DEFAULT NULL,
  `comment_page` int(11) DEFAULT NULL,
  `comment_content` text,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hotel_comment_log
-- ----------------------------
DROP TABLE IF EXISTS `hotel_comment_log`;
CREATE TABLE `hotel_comment_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `spidered_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hotel_log
-- ----------------------------
DROP TABLE IF EXISTS `hotel_log`;
CREATE TABLE `hotel_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `spidered_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mm_jpg
-- ----------------------------
DROP TABLE IF EXISTS `mm_jpg`;
CREATE TABLE `mm_jpg` (
  `mm_id` varchar(50) NOT NULL,
  `mm_url` varchar(200) DEFAULT NULL,
  `mm_page` int(11) DEFAULT NULL,
  `mm_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mm_jpg_image_log
-- ----------------------------
DROP TABLE IF EXISTS `mm_jpg_image_log`;
CREATE TABLE `mm_jpg_image_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `spidered_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mm_jpg_log
-- ----------------------------
DROP TABLE IF EXISTS `mm_jpg_log`;
CREATE TABLE `mm_jpg_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `spidered_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
