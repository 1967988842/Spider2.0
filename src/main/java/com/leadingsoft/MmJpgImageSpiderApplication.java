package com.leadingsoft;

import org.cef.CefApp;
import org.cef.CefApp.CefAppState;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.OS;
import org.cef.handler.CefAppHandlerAdapter;

import com.leadingsoft.controller.browser.impl.MmJpgImageBrowserInstance;
import com.leadingsoft.core.AddLibraryDir;

/**
 * @ClassName CtripHotelSpiderApplication
 * @Description 抓取携程酒店信息的例子
 * @author gongym
 * @date 2018年6月20日 下午12:14:23
 */
public class MmJpgImageSpiderApplication {
	public static void main(String[] args) {
		CefApp.addAppHandler(new CefAppHandlerAdapter(null) {
			@Override
			public void stateHasChanged(org.cef.CefApp.CefAppState state) {
				if (state == CefAppState.TERMINATED)
					System.exit(0);
			}
		});
		AddLibraryDir.addDLL();
		boolean osrEnabledArg = OS.isLinux();
		boolean transparentPaintingEnabledArg = false;
		CefSettings settings = new CefSettings();
		settings.windowless_rendering_enabled = osrEnabledArg;
		settings.background_color = settings.new ColorType(100, 255, 242, 211);
		CefApp myApp = CefApp.getInstance(settings);
		final CefClient client = myApp.createClient();
		// 新建一个浏览器实例进行抓取
		MmJpgImageBrowserInstance browserInstanceFirst = new MmJpgImageBrowserInstance(client, osrEnabledArg,
				transparentPaintingEnabledArg);
		browserInstanceFirst.createBrowserAndCrawler(1L, 15L);
	}
}
