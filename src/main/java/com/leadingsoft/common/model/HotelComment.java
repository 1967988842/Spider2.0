package com.leadingsoft.common.model;

public class HotelComment {
	private Long commentId;
	private String hotelId;
	private Integer commentPage;
	private String commentContent;

	public Long getCommentId() {
		return commentId;
	}
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId == null ? null : hotelId.trim();
	}
	public Integer getCommentPage() {
		return commentPage;
	}
	public void setCommentPage(Integer commentPage) {
		this.commentPage = commentPage;
	}
	public String getCommentContent() {
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent == null ? null : commentContent.trim();
	}
}
