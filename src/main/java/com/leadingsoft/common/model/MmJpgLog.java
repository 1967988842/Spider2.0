package com.leadingsoft.common.model;

public class MmJpgLog {
	private Long logId;
	private String spideredUrl;

	public Long getLogId() {
		return logId;
	}
	public void setLogId(Long logId) {
		this.logId = logId;
	}
	public String getSpideredUrl() {
		return spideredUrl;
	}
	public void setSpideredUrl(String spideredUrl) {
		this.spideredUrl = spideredUrl == null ? null : spideredUrl.trim();
	}
}
