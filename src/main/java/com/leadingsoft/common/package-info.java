/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.common 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月4日 上午11:54:36 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 通用工具库
 * @author gongym
 * @date 2018年6月4日 上午11:54:36
 */
package com.leadingsoft.common;
