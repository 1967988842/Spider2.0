package com.leadingsoft.controller.download;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.SQLManager;
import org.cef.browser.CefBrowser;
import org.cef.callback.CefStringVisitor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.leadingsoft.common.TaskQuene;
import com.leadingsoft.common.model.MmJpgImageLog;

/**
 * @ClassName DownloadMmJpgImage
 * @Description 按照抓好的妹子ID下载每一个妹子的所有图片
 * @author gongym
 * @date 2018年6月22日 下午12:19:58
 */
public class DownloadAllMmJpgImage implements CefStringVisitor {
	private static Logger logger = LoggerFactory.getLogger(DownloadAllMmJpgImage.class);
	private CefBrowser browser;
	private String listSelector;
	private SQLManager sqlManager;
	private List<String> mmJpgIdList;

	public DownloadAllMmJpgImage(CefBrowser browser, String listSelector, SQLManager sqlManager,
			List<String> mmJpgIdList) {
		this.browser = browser;
		this.listSelector = listSelector;
		this.sqlManager = sqlManager;
		this.mmJpgIdList = mmJpgIdList;
	}
	@Override
	public void visit(String string) {
		// 执行一个JavaScript代
		// 然后页面会加载所有图片
		// 然后获取所有的URL，进行下载
		browser.executeJavaScript("", "", 0);
		// 等待页面加载完毕
		String url = browser.getURL();
		logger.debug("开始下载图片，URL：{}", url);
		Document htmlDocument = Jsoup.parse(string);
		// 需要取出图片地址
		Elements elementsByClass = htmlDocument.getElementsByClass(listSelector);
		if (null != elementsByClass && elementsByClass.size() > 0) {
			MmJpgImageLog mmJpgImageLogTemplate = new MmJpgImageLog();
			mmJpgImageLogTemplate.setSpideredUrl(url);
			MmJpgImageLog mmJpgImageLog = sqlManager.templateOne(mmJpgImageLogTemplate);
			if (null == mmJpgImageLog) {
				logger.debug("当前页面的图片没有下载过，开始下载");
			}
		}
		// 无论如何都要开始下一页的页面加载
		logger.debug("保存当前页面的图片完毕，开始保存日志");
		MmJpgImageLog nowMmJpgImageLog = new MmJpgImageLog();
		nowMmJpgImageLog.setSpideredUrl(url);
		sqlManager.insert(nowMmJpgImageLog);
		logger.debug("插入当前URL到日志表");
		// 需要判断是否有下一页
		Boolean isNext = false;
		String nextUrl = TaskQuene.getMmJpgImageUrl(isNext, url, mmJpgIdList);
		if (StringUtils.isNotEmpty(nextUrl)) {
			TaskQuene.mmJpgTaskUrl.add(nextUrl.toString());
			logger.debug("获取到下一页URL为：{}", nextUrl);
			logger.debug("当前队列中的URL为：{}", TaskQuene.mmJpgTaskUrl);
			logger.debug("===================={}任务结束====================", nextUrl);
		} else {
			// 当前任务结束。关闭浏览器
			logger.debug("未获取到下一页URL，抓取任务结束");
			browser.close();
		}
	}
}
