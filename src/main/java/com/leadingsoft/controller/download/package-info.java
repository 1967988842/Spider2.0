/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title: package-info.java 
 * @Prject: Spider2.0
 * @Package: com.leadingsoft.controller.download 
 * @Description: 新版爬虫，使用Java调用浏览器引擎
 * @author: gongym   
 * @date: 2018年6月12日 下午2:31:06 
 * @version: V1.0   
 */
/**
 * @ClassName: package-info
 * @Description: 下载当前Html文档
 * @author: gongym
 * @date: 2018年6月12日 下午2:31:06
 */
package com.leadingsoft.controller.download;
