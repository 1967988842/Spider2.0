package com.leadingsoft.controller.parse;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.SQLManager;
import org.cef.browser.CefBrowser;
import org.cef.callback.CefStringVisitor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.leadingsoft.common.Constant;
import com.leadingsoft.common.TaskQuene;
import com.leadingsoft.common.model.MmJpg;
import com.leadingsoft.common.model.MmJpgLog;
import com.leadingsoft.controller.parse.impl.GetMmJpg;

/**
 * @ClassName ParsingMmjpg
 * @Description 解析页面保存妹子ID和主页地址
 * @author gongym
 * @date 2018年6月22日 上午12:14:43
 */
public class ParsingMmJpg implements CefStringVisitor {
	private static Logger logger = LoggerFactory.getLogger(ParsingMmJpg.class);
	private CefBrowser browser;
	private String listSelector;
	private SQLManager sqlManager;
	private Long startIndex;
	private Long stopIndex;

	public ParsingMmJpg(CefBrowser browser, String listSelector, SQLManager sqlManager, Long startIndex,
			Long stopIndex) {
		this.browser = browser;
		this.listSelector = listSelector;
		this.sqlManager = sqlManager;
		this.startIndex = startIndex;
		this.stopIndex = stopIndex;
	}
	@Override
	public void visit(String string) {
		String url = browser.getURL();
		logger.debug("开始解析当前URL：{}", url);
		Document htmlDocument = Jsoup.parse(string);
		Elements elementsByClass = htmlDocument.getElementsByClass(listSelector);
		// 检查当前URL是否被解析过了
		MmJpgLog mmJpgLogTemplate = new MmJpgLog();
		mmJpgLogTemplate.setSpideredUrl(url);
		MmJpgLog mmJpgLog = sqlManager.templateOne(mmJpgLogTemplate);
		if (null == mmJpgLog) {
			logger.debug("当前页面没有被解析过，调用解析方法进行解析");
			GetMmJpg getMmJpg = new GetMmJpg();
			List<Object> mmJpgList = getMmJpg.elementsToObjects(elementsByClass);
			String pageIndex = StringUtils.remove(url, Constant.MMJPGLISTURLTEMP);
			mmJpgList.stream().filter(object -> object instanceof MmJpg).forEach(object -> {
				MmJpg mmJpg = (MmJpg) object;
				if (StringUtils.isNotBlank(pageIndex)) {
					String newPageIndex = StringUtils.remove(pageIndex, "home/");
					mmJpg.setMmPage(Integer.parseInt(newPageIndex));
				} else {
					mmJpg.setMmPage(1);
				}
			});
			logger.debug("当前页面解析成功，当前URL为：{}，当前页为：{}，共获取到：{}条数据", url, pageIndex, mmJpgList.size());
			sqlManager.insertBatch(MmJpg.class, mmJpgList);
			logger.debug("保存解析列表数据到数据库成功");
			MmJpgLog nowMmJpgLog = new MmJpgLog();
			nowMmJpgLog.setSpideredUrl(url);
			sqlManager.insert(nowMmJpgLog);
			logger.debug("插入当前URL到日志表");
		}
		logger.debug("当前页面已经解析完毕，开始获下一页URL");
		String nextUrl = TaskQuene.getMmJpgListUrl(url, startIndex, stopIndex);
		if (StringUtils.isNotEmpty(nextUrl)) {
			TaskQuene.mmJpgTaskUrl.add(nextUrl.toString());
			logger.debug("获取到下一页URL为：{}", nextUrl);
			logger.debug("当前队列中的URL为：{}", TaskQuene.mmJpgTaskUrl);
			logger.debug("===================={}任务结束====================", nextUrl);
		} else {
			// 当前任务结束。关闭浏览器
			logger.debug("未获取到下一页URL，抓取任务结束");
			browser.close();
		}
	}
}
