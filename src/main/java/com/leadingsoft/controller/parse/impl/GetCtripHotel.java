package com.leadingsoft.controller.parse.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.leadingsoft.common.model.Hotel;
import com.leadingsoft.controller.parse.GetDocumentData;

/**
 * @ClassName GetCtripHotelList
 * @Description 例子1、抓取携程北京地区酒店列表
 * @author gongym
 * @date 2018年6月7日 下午5:16:52
 */
public class GetCtripHotel implements GetDocumentData {
	@Override
	public Object elementToObject(Element element) {
		return null;
	}
	@Override
	public List<Object> elementsToObjects(Elements elements) {
		List<Object> hotelList = new ArrayList<Object>();
		elements.forEach((document) -> {
			Hotel hotel = new Hotel();
			// 酒店ID===hotelId
			Elements hotelIdElement = document.select("div.hotel_new_list");
			String hotelId = hotelIdElement.attr("id");
			hotel.setHotelId(hotelId);
			// 酒店名称===hotelName
			Elements hotelNameElement = document.select("ul.hotel_item>li.hotel_item_name>h2>a");
			String hotelName = hotelNameElement.attr("title");
			hotel.setHotelName(hotelName);
			// 是否为战略合作酒店===hotelStrategymedal
			Elements hotelStrategymedalElement = document
					.select("ul.hotel_item>li.hotel_item_name>span>span.hotel_strategymedal");
			if (null != hotelStrategymedalElement) {
				hotel.setHotelStrategymedal(1);
			} else {
				hotel.setHotelStrategymedal(0);
			}
			// 酒店地址===hotelAddress
			Elements hotelAddressElement = document.select("p.hotel_item_htladdress");
			if (null != hotelAddressElement && hotelAddressElement.size() > 0) {
				String hotelAddress = hotelAddressElement.get(0).text();
				hotel.setHotelAddress(hotelAddress);
			}
			// 酒店基础价格===hotelPrice
			Elements hotelPriceElement = document.select("span.J_price_lowList");
			if (null != hotelPriceElement && hotelPriceElement.size() > 0) {
				String hotelPrice = hotelPriceElement.get(0).text();
				hotel.setHotelPrice(Integer.parseInt(hotelPrice));
			}
			// 酒店标签===hotelSpecialLabel
			Elements hotelSpecialLabelElements = document.select("i.i_label");
			StringBuilder hotelSpecialLabel = new StringBuilder();
			hotelSpecialLabelElements.forEach((hotelSpecialLabelElement) -> {
				hotelSpecialLabel.append(hotelSpecialLabelElement.text()).append(",");
			});
			if (StringUtils.isNoneBlank(hotelSpecialLabel.toString())) {
				String hotelSpecialLabelResult = hotelSpecialLabel.toString().substring(0,
						hotelSpecialLabel.toString().length() - 1);
				hotel.setHotelSpecialLabel(hotelSpecialLabelResult);
			}
			// 酒店设施===hotelFacility
			Elements hotelFacilityElements = document.select("div.icon_list>i");
			StringBuilder hotelFacility = new StringBuilder();
			hotelFacilityElements.forEach((hotelFacilityElement) -> {
				hotelFacility.append(hotelFacilityElement.attr("title")).append(",");
			});
			if (StringUtils.isNoneBlank(hotelFacility.toString())) {
				String hotelFacilityResult = hotelFacility.toString().substring(0,
						hotelFacility.toString().length() - 1);
				hotel.setHotelFacility(hotelFacilityResult);
			}
			// 酒店等级===hotelLevel
			Elements hotelLevelElement = document.select("span.hotel_level");
			if (null != hotelLevelElement && hotelLevelElement.size() > 0) {
				String hotelLevel = hotelLevelElement.get(0).text();
				hotel.setHotelLevel(hotelLevel);
			}
			// 酒店评分===hotelValue
			Elements hotelValueElement = document.select("span.hotel_value");
			if (null != hotelValueElement && hotelValueElement.size() > 0) {
				String hotelValue = hotelValueElement.get(0).text();
				hotel.setHotelValue(Float.parseFloat(hotelValue));
			}
			// 用户推荐比===hotelTotalJudgementScore
			Elements hotelTotalJudgementScoreElement = document.select("span.total_judgement_score>span");
			if (null != hotelTotalJudgementScoreElement && hotelTotalJudgementScoreElement.size() > 0) {
				String hotelTotalJudgementScore = hotelTotalJudgementScoreElement.get(0).text();
				String newHotelTotalJudgementScore = hotelTotalJudgementScore.replace("%", "");
				hotel.setHotelTotalJudgementScore(Integer.parseInt(newHotelTotalJudgementScore));
			}
			// 评论用户数===hotelJudgement
			Elements hotelJudgementElement = document.select("span.hotel_judgement>span");
			if (null != hotelJudgementElement && hotelJudgementElement.size() > 0) {
				String hotelJudgement = hotelJudgementElement.get(0).text();
				hotel.setHotelJudgement(Integer.parseInt(hotelJudgement));
			}
			// 评论摘要===hotelRecommend
			Elements hotelRecommendElement = document.select("span.recommend");
			if (null != hotelRecommendElement && hotelRecommendElement.size() > 0) {
				String hotelRecommend = hotelRecommendElement.get(0).text();
				String newHotelRecommend = StringUtils.replaceEach(hotelRecommend, new String[] { "“", "”", "<br>" },
						new String[] { "", "", "" });
				hotel.setHotelRecommend(newHotelRecommend);
			}
			hotelList.add(hotel);
		});
		return hotelList;
	}
}
