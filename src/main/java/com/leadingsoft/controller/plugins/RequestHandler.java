package com.leadingsoft.controller.plugins;

import java.util.Vector;

import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefRequestHandlerAdapter;
import org.cef.network.CefPostData;
import org.cef.network.CefPostDataElement;
import org.cef.network.CefRequest;
import org.cef.network.CefResponse;
import org.cef.network.CefURLRequest.Status;

public class RequestHandler extends CefRequestHandlerAdapter {
	public void onResourceLoadComplete(CefBrowser browser, CefFrame frame, CefRequest request, CefResponse response,
			Status status, long what) {
		CefPostData postData = request.getPostData();
		if (null != postData) {
			Vector<CefPostDataElement> elements = new Vector<CefPostDataElement>();
			postData.getElements(elements);
			for (CefPostDataElement el : elements) {
				int numBytes = el.getBytesCount();
				if (numBytes <= 0)
					continue;
				byte[] readBytes = new byte[numBytes];
				if (el.getBytes(numBytes, readBytes) <= 0)
					continue;
				// String readString = new String(readBytes).trim();
			}
		}
	}
}
