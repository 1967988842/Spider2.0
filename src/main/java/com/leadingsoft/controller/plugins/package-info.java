/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.controller.plugins 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月11日 上午10:45:46 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description JCEF自定义处理器插件 <br>
 *              PS：现在才发现这么个东西，如果早知道就会少走很多弯路了
 * @author gongym
 * @date 2018年6月11日 上午10:45:46
 */
package com.leadingsoft.controller.plugins;
