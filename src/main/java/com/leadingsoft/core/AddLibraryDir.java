package com.leadingsoft.core;

import java.lang.reflect.Field;

import com.leadingsoft.common.Constant;

public class AddLibraryDir {
	/**
	 * @Title addLibraryDir
	 * @Description 添加DLL路径
	 * @param libraryPath
	 * @return void
	 */
	public static void addDLL() {
		try {
			String libraryPath = Constant.DLLPATH;
			Field userPathsField = ClassLoader.class.getDeclaredField("usr_paths");
			userPathsField.setAccessible(true);
			String[] paths = (String[]) userPathsField.get(null);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < paths.length; i++) {
				if (libraryPath.equals(paths[i])) {
					continue;
				}
				sb.append(paths[i]).append(';');
			}
			sb.append(libraryPath);
			System.setProperty("java.library.path", sb.toString());
			final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
			sysPathsField.setAccessible(true);
			sysPathsField.set(null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
