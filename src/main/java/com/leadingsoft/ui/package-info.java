/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.ui 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月5日 下午11:13:16 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 浏览器窗口以及，页面元素（使用swing--太丑了）
 * @author gongym
 * @date 2018年6月5日 下午11:13:16
 */
package com.leadingsoft.ui;
